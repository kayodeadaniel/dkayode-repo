#!/bin/bash

# Output file path

#Name: daniel Kayode
#The script gathers server info and output the results to a file called serverinfo.info under /tmp




output_file="/tmp/serverinfo.info"

# Get the current date
current_date=$(date)

echo "Current Date: $current_date" > "$output_file"

echo >> "$output_file"

# Get the last 10 users who logged in to the server
last_users=$(last -n 10)

echo "Last 10 Users:" >> "$output_file"
echo "$last_users" >> "$output_file"

echo >> "$output_file"

# Get swap space information
swap_info=$(free -h | grep Swap)

echo "Swap Space:" >> "$output_file"
echo "$swap_info" >> "$output_file"

echo >> "$output_file"

# Get kernel version
kernel_version=$(uname -srm)

echo "Kernel Version: $kernel_version" >> "$output_file"

echo >> "$output_file"

# Get IP address
ip_address=$(hostname -I)

echo "IP Address: $ip_address" >> "$output_file"

